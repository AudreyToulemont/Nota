# Natural
A theme for [conky](https://github.com/brndnmtthws/conky). <br />
It displays custom **notes**.

## Preview
![Preview](preview.png)

## Installation

Install conky: <br />
Ubuntu: `apt-get install conky-all` <br/>
Fedora: `yum install conky` <br />
Other: see your local documentation or try the Conky documentation.

Clone this repo:
```
git clone git@gitlab.com:AudreyToulemont/Nota.git
```
Copy .conkyrc, and .conky/ to your home directory:
```
cd Nota
cp .conkyrc ~/
cp -r .conky ~/
```
Copy the font to your fonts directory:
```
cp Amaranth-Regular.otf ~/.fonts
```
Now you are ready to run conky. Open a terminal and run the following:
```
conky -d
```

To run conky at startup, go to System > Preferences > Startup Applications, click "Add" and add the path to the conkyStart file (/usr/share/conkycolors/bin/conkyStart)

## Edit notes
Edit the file *notes.txt* wich is in *.conky* folder. It will update automatically. <br />

**Note:** <br />
You can create a custom keyboard shortcut that will open immediatly your file *notes.txt* to modify directly your notes (that's what I have done).
